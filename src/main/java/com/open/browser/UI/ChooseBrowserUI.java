package com.open.browser.UI;

import com.open.browser.service.AgentService;
import com.open.browser.service.CustomerOfferLink;
import com.open.browser.service.ServerService;
import com.open.browser.service.RestService;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.codehaus.jettison.json.JSONException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;


/**
 * Created by ppetrov on 01.07.2017.
 */
public class ChooseBrowserUI extends Application {
    private RestService agentService = new RestService();
    private GridPane gpnael = new GridPane();

    @Override
    public void start(Stage primaryStage) throws IOException {
        Button openAgentBtn = new Button();
        Button customerOfferBtn = new Button();

        final ComboBox serverBox = new ComboBox();


        serverBox.getItems().addAll(
                new ServerService().getServerDTONameList()
        );
        serverBox.getSelectionModel().selectFirst();

        final ComboBox agentBox = new ComboBox();
        agentBox.getItems().addAll(
                new AgentService().getAgentNames()
        );
        agentBox.getSelectionModel().selectFirst();
        openAgentBtn.setText("Open browser");
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        openAgentBtn.setOnAction(event -> {
            try {
                System.out.println(serverBox.getValue());
                String token = agentService.getToken(serverBox.getValue().toString(), agentBox.getValue().toString());
                WebDriver webDriver = new ChromeDriver();

                webDriver.get(new ServerService().getServerByName(serverBox.getValue().toString()).getBasicAuthUrl() + "/dsd-dealer/auth?token=" + token);
            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }
        });

        customerOfferBtn.setText("Get offer link");
        customerOfferBtn.setOnAction(event -> {
            try {
                String retailCycleID = new CustomerOfferLink().getRetailCycleID(serverBox.getValue().toString());
                System.out.println(retailCycleID);
                WebDriver webDriver = new ChromeDriver();
                webDriver.get(new ServerService().getServerByName(serverBox.getValue().toString()).getBasicAuthUrl() + "/dsd-customer/#/login?offerHash=" + retailCycleID );
            } catch ( IOException | JSONException e) {
                e.printStackTrace();
            }
        });

        Scene scene = new Scene(new Group(), 550, 100);
        primaryStage.setTitle("Browser chooser");
        gpnael.setVgap(4);
        gpnael.setHgap(10);
        gpnael.add(openAgentBtn, 5, 1);
        gpnael.add(customerOfferBtn, 7, 1);
        gpnael.add(serverBox, 1, 1);
        gpnael.add(agentBox, 3, 1);
        Group root = (Group) scene.getRoot();
        root.getChildren().add(gpnael);

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }


}
