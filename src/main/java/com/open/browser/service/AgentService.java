package com.open.browser.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.open.browser.dto.AgentDTO;
import com.open.browser.dto.ServerDTO;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ppetrov on 02.07.2017.
 */
public class AgentService {

    private List<AgentDTO> getServerDtOList() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
       return mapper.readValue(this.getClass().getResourceAsStream("/files/agent-information.json")
                , mapper.getTypeFactory().constructCollectionType(List.class, AgentDTO.class));

    }

    public List<String> getAgentNames() throws IOException {
        List<AgentDTO> agentDTOS = getServerDtOList();
        List<String> strings = new ArrayList<>();
        for (AgentDTO agentDTO: agentDTOS) {
            if (agentDTO != null) {
                strings.add(agentDTO.getLogin());
            }
        }
        return  strings;
    }

    public AgentDTO getAgentByName(String name) throws IOException {
        List<AgentDTO> agentDTOS = getServerDtOList();
        AgentDTO agentDTO = null;
        for (AgentDTO agent: agentDTOS) {
            if (name.equals(agent.getLogin())) {
                agentDTO = agent;
            }
        }
        return agentDTO;
    }
}
