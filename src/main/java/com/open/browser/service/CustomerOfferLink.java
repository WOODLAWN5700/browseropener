package com.open.browser.service;

import com.open.browser.dto.ServerDTO;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.*;
import java.util.Base64;


/**
 * Created by ppetrov on 02.07.2017.
 */
public class CustomerOfferLink {
    public String getRetailCycleID(String serverName) throws JSONException, IOException {
        ServerDTO dto = new ServerService().getServerByName(serverName);
        String basePath = dto.getUrl();
        String basicAuthLogin = dto.getBasicAuthLogin();
        String basicAuthPassword = dto.getBasicAuthPassword();

        String result = null;
        StringBuilder buffer = new StringBuilder();
        try (BufferedReader jsonReader = new BufferedReader (new InputStreamReader(
               (this.getClass().getResourceAsStream("/files/retail-config.json")), "UTF-8"))) {
            while ((result = jsonReader.readLine()) != null) {
                buffer.append(result);
            }
        }
        StringEntity params = new StringEntity(
                new JSONObject(buffer.toString()).toString());
        HttpPost post = new HttpPost(String.format("%s/osm-api/external/api/v1/retail-config", basePath));
        setProxy(post, basePath);
        HttpClient client = HttpClientBuilder.create().build();
        params.setContentType("application/json");
        post.setEntity(params);
        post.setHeader("Osm-Api-Auth",
                "ODNhM2Y5OTJlOGZlNDk0ZmIxNTFhNGI0OTVlMjVlOTk6Y2QzMmU4MDEtNDcwYy00MDc4LWIwODQtYTdjOWFmM2IyMDZl");
        setBasicAuthHeader(post, basicAuthLogin, basicAuthPassword);
        //Execute
        HttpResponse httpResponse = client.execute(post);
        System.out.println(httpResponse);
        System.out.println("REQUEST " + httpResponse.toString());

        return new JSONObject(new BasicResponseHandler()
                .handleResponse(httpResponse))
                .get("retailCycleId").toString();
    }

    private static void setBasicAuthHeader(HttpRequestBase requestBase, String basicAuthLogin, String basicAuthPassword) {
        String authStr = basicAuthLogin + ":" + basicAuthPassword;
        String authEncoded = Base64.getEncoder().encodeToString(authStr.getBytes());
        requestBase.setHeader("Authorization", "Basic " + authEncoded);
    }

    private static void setProxy(HttpRequestBase request, String basePath) {
        if (!basePath.equals("http://bmw.cn.osm.loc")) {
            HttpHost proxy = new HttpHost("proxy.t-systems.ru", 3128, "http");
            request.setConfig(RequestConfig.custom()
                    .setProxy(proxy)
                    .build());
        }
    }

}
