package com.open.browser.service;

import com.open.browser.dto.AgentDTO;
import com.open.browser.dto.ServerDTO;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;


/**
 * Created by ppetrov on 01.07.2017.
 */
public class RestService {


    public String getToken(String serverName, String agentName) throws JSONException, IOException {
        // Use XSRF-TOKEN cookie as X-XSRF-TOKEN header for further requests
        ServerDTO dto = new ServerService().getServerByName(serverName);
        AgentDTO agentDTO = new AgentService().getAgentByName(agentName);
        String basePath = dto.getUrl();
        String basicAuthLogin = dto.getBasicAuthLogin();
        String basicAuthPassword = dto.getBasicAuthPassword();
        String rootDealerInternalUrl = String.format("%s/dsd-dealer/internal/api/v1", basePath);

        System.out.println(basePath);
        System.out.println(basicAuthLogin);
        System.out.println(basicAuthPassword);



        JSONObject json = new JSONObject();
        json.put("salutation", agentDTO.getSalutation());
        json.put("login", agentDTO.getLogin());
        json.put("givenname", agentDTO.getGivenname());
        json.put("surname", agentDTO.getSurname());
        json.put("email", agentDTO.getEmail());
        json.put("phone", agentDTO.getPhone());
        json.put("preferredLanguage", agentDTO.getPreferredLanguage());
        List<String> bussinedsList = agentDTO.getBusinessPartnerIds();
        String[] partners = new String[bussinedsList.size()];
        for(int i = 0; i < bussinedsList.size(); i++) {
            partners[i] = bussinedsList.get(i);
        }
        json.put("businessPartnerIds", Arrays.asList(partners));

        List<String> rolesList = agentDTO.getRoles();
        String[] roles = new String[rolesList.size()];
        for(int i = 0; i < rolesList.size(); i++) {
            roles[i] = rolesList.get(i);
        }
        json.put("roles", Arrays.asList(roles));

        StringEntity params = new StringEntity(json.toString());
        params.setContentType("application/json");

        HttpPost post = new HttpPost(String.format("%s/sgate/auth", rootDealerInternalUrl));
        HttpClient client = HttpClientBuilder.create().build();
        post.setHeader("Osm-Api-Auth",
                "ZGI5OTcyZGU3NGI0NDMzZmEyMWQ4MWFiMTE0ODYzZjM6OGM2MzQzYTgtMjM0Mi00NWFkLTg4ZTUtNTk1OTNhOWNmMjYw");
        setBasicAuthHeader(post, basicAuthLogin, basicAuthPassword);
        post.setEntity(params);
        setProxy(post, basePath);
        //execute ...
        HttpEntity entity8 = client.execute(post).getEntity();
        String responseString = null;
        if (entity8 != null) {
            responseString = EntityUtils.toString(entity8, "UTF-8");
            System.out.println("Response body: " + responseString);
        } else {
            assert (false);
        }
        return responseString;
    }

    private static void setBasicAuthHeader(HttpRequestBase requestBase, String basicAuthLogin, String basicAuthPassword) {
        String authStr = basicAuthLogin + ":" + basicAuthPassword;
        String authEncoded = Base64.getEncoder().encodeToString(authStr.getBytes());
        requestBase.setHeader("Authorization", "Basic " + authEncoded);
    }

    private static void setProxy(HttpRequestBase request, String basePath) {
        if (!basePath.equals("http://bmw.cn.osm.loc")) {
            HttpHost proxy = new HttpHost("proxy.t-systems.ru", 3128, "http");
            request.setConfig(RequestConfig.custom()
                    .setProxy(proxy)
                    .build());
        }
    }

}
