package com.open.browser.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.open.browser.dto.ServerDTO;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ppetrov on 02.07.2017.
 */
public class ServerService {

    private List<ServerDTO> getServerDtOList() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(this.getClass().getResourceAsStream("/files/server-Information.json")
                , mapper.getTypeFactory().constructCollectionType(List.class, ServerDTO.class));
    }

    public List<String> getServerDTONameList() throws IOException {
        List<String> strings = new ArrayList<>();
        List<ServerDTO> serverDTOS = getServerDtOList();
        for (ServerDTO dto: serverDTOS){
            strings.add(dto.getName());
        }
        return  strings;
    }

    public ServerDTO getServerByName(String name) throws IOException {
        List<ServerDTO> serverDTOS = getServerDtOList();
        ServerDTO result = null;
        for (ServerDTO dto: serverDTOS) {
            if (dto.getName().equals(name)) {
                result = dto;
            }
        }
        return result;
    }
}
