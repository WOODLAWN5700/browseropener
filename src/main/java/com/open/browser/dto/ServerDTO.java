package com.open.browser.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by ppetrov on 01.07.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class ServerDTO {
    private String name;
    private String url;
    private String basicAuthLogin;
    private String basicAuthPassword;
    private String basicAuthUrl;
}
