package com.open.browser.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by ppetrov on 02.07.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class AgentDTO {
    private String salutation;
    private String login;
    private String givenname;
    private String surname;
    private String email;
    private String phone;
    private String preferredLanguage;
    private List<String> businessPartnerIds;
    private List<String> roles;
}
